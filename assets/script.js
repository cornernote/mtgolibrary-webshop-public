// ajax links
jQuery(document).on('click', 'a.webshop-ajax', function (e) {
    e.preventDefault();
    var $webshop = jQuery('#webshop-table');
    $webshop.css('opacity', 0.4);
    jQuery.get(this.href, {}, function (response) {
        $webshop.html(jQuery(response).find('#webshop-table')).css('opacity', 1);
    });
});

// ajax forms
jQuery(document).on('keyup', 'form.webshop-ajax', function (e) {
    var $webshop = jQuery('#webshop-table'),
        $form = jQuery(this);
    delay(function () {
        $webshop.css('opacity', 0.4);
        jQuery.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: $form.serializeArray(),
            success: function (response) {
                $webshop.html(jQuery(response).find('#webshop-table')).css('opacity', 1);
                var $search = jQuery('#webshop-search'),
                    tmp = $search.val();
                $search.focus().val('').val(tmp);
            }
        });
    }, 500);
});

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();