<?php
/**
 * Plugin Name: MTGO Library - Web Shop
 * Description: Provides a Web Shop for MTGO Library Bots
 * Version: 1.0.0
 * Author: Mr PHP
 * Author URI: http://mrphp.com.au
 * Text Domain: mtgolibrary_webshop
 * License: Copyright (c) 2014
 */

/**
 *
 * Copyright (c) 2014, Mr PHP <info@mrphp.com.au>
 * All rights reserved.
 *  _____     _____ _____ _____
 * |     |___|  _  |  |  |  _  |
 * | | | |  _|   __|     |   __|
 * |_|_|_|_| |__|  |__|__|__|
 *
 */

// do not allow direct entry here
if (!function_exists('wp')) {
    echo 'cannot be called directly';
    exit;
}

// define constants
define('MTGOLIBRARY_WEBSHOP_VERSION', '1.0.0');
define('MTGOLIBRARY_WEBSHOP_URL', plugin_dir_url(__FILE__));
define('MTGOLIBRARY_WEBSHOP_DIR', plugin_dir_path(__FILE__));

// load plugin
require_once(MTGOLIBRARY_WEBSHOP_DIR . 'includes/MtgolibraryWebshop.php');

